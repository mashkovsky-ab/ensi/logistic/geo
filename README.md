# Geo service

Сервис для работы с географическими данными

## Требования

### Требования к ПО

- docker
- composer
- npm

### Общие требования Ensi

https://www.notion.so/ensibok

## Разворот локально

Разворот сервиса на локальной машине описан в [документации Ensi](https://greensight.atlassian.net/wiki/spaces/ENSI/pages/362676232/Backend-)

## Зависимости

| Название | Описание  | Переменные окружения |
|---|---|---|
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
